- title: "Owner permissions are required to update Package settings"  # (required) Actionable title. e.g., The `confidential` field for a `Note` is deprecated. Use `internal` instead.
  announcement_milestone: "15.8"  # (required) The milestone when this feature was first announced as deprecated.
  removal_milestone: "16.0"  # (required) The milestone when this feature is planned to be removed
  breaking_change: true  # (required) If this deprecation is a breaking change, set this value to true
  reporter: trizzi  # (required) GitLab username of the person reporting the deprecation
  stage: Package  # (required) String value of the stage that the feature was created in. e.g., Growth
  issue_url: https://gitlab.com/gitlab-org/gitlab/-/issues/370471  # (required) Link to the deprecation issue in GitLab
  body: |  # (required) Do not modify this line, instead modify the lines below.
    You must have Owner permissions to use the GitLab UI to update the `Packages and registries` settings for your groups. This includes [allowing or preventing duplicate package uploads](https://docs.gitlab.com/ee/user/packages/maven_repository/#do-not-allow-duplicate-maven-packages), [package request forwarding](https://docs.gitlab.com/ee/user/packages/maven_repository/#request-forwarding-to-maven-central), and [enabling lifecycle rules for the Dependency Proxy](https://docs.gitlab.com/ee/user/packages/dependency_proxy/reduce_dependency_proxy_storage.html). Currently, you can use the GraphQL API with Owner or Maintainer permissions to update these settings.

    In GitLab 16.0 and later, you must have Owner permissions to use the GraphQL API to change the `Packages and registries` settings.
